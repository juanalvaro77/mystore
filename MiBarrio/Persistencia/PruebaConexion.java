package Persistencia;
import java.sql.*;
import javax.swing.JOptionPane;

public class PruebaConexion
{
    public void crearConexion()
    {
        try
        {
            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/minticciclo3","root","");
            Statement myStmt = myConn.createStatement();
            ResultSet myRs = myStmt.executeQuery("select * from password");
            while(myRs.next())
            {
                System.out.println(myRs.getString("ID")+", "+myRs.getString("user")+", "+myRs.getString("pass"));
                
            }
        }
        catch (Exception exc) 
        {
            exc.printStackTrace();
        }
    
    }
    public void ingresarDato()
    {
        try
        {
            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/minticciclo3","root","");
            Statement myStmt = myConn.createStatement();
            String sql = "INSERT INTO `password`(`user`, `pass`, `comentario`) VALUES ('888888','888888','Usuario solo vista')";
            myStmt.executeUpdate(sql);
            System.out.println("Usuario Ingresado");
        }
        catch (Exception exc) 
        {
            exc.printStackTrace();
        }
    
    }
    public void editarDato()
    {
        try
        {
            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/minticciclo3","root","");
            Statement myStmt = myConn.createStatement();
            String usuario = JOptionPane.showInputDialog(null, "Por favor volver a digitar Usuario: ");
            String clave =  JOptionPane.showInputDialog(null, "Ingrese nueva Clave: ");
            String sql = "UPDATE password SET pass=clave WHERE user=usuario";
            myStmt.executeUpdate(sql);
            System.out.println("Dato modificado");
        }
        catch (Exception exc) 
        {
            exc.printStackTrace();
        }
    
    }
    public void borrarDato()
    {
        try
        {
            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/minticciclo3","root","");
            Statement myStmt = myConn.createStatement();
            String sql = "DELETE FROM `password` WHERE user='888888'";
//            int rowsAffected = myStmt.executeUpdate(sql);
//            System.out.println("Fila intervenida: " + rowsAffected);
            System.out.println("Dato Eliminado");
        }
        catch (Exception exc) 
        {
            exc.printStackTrace();
        }
    
    }
    
}
